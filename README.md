# otto

A very basic CPU-based rasterizer done purely for fun and educational reasons. The final rendered pictures:

![Basic Shapes](images/basic_shapes.png "Basic Shapes")
![Four Spheres](images/four_spheres.png "Four Spheres")
![Four Spheres (Wireframe)](images/four_spheres_wireframe.png "Four Spheres (Wireframe)")
