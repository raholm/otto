#pragma once

#include <vector>

#include <otto/math/math.h>

namespace otto {

  struct Model
  {
    struct Triangle
    {
      unsigned int v1;
      unsigned int v2;
      unsigned int v3;
    };

    std::vector<Vector3_F32> vertices;
    std::vector<Triangle> triangles;
  };

  Model GetCubeModel();
  Model GetSphereModel();

}  // otto
