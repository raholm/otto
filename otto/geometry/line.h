#pragma once

#include <otto/math/math.h>

namespace otto {

  struct Line
  {
    Vector2_F32 start;
    Vector2_F32 end;
  };

  struct Line3
  {
    Vector3_F32 start;
    Vector3_F32 end;
  };

}  // otto
