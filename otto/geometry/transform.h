#pragma once

#include <otto/math/math.h>

namespace otto {

  struct Transform
  {
    float Rotation() const;

    Vector3_F32 position;
    float scale;
    float rotation;
  };

}  // otto
