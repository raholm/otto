#pragma once

#include <otto/math/math.h>

namespace otto {

  struct Sphere
  {
    Vector3_F32 center;
    float radius { 0.0f };
  };

}  // otto
