#pragma once

#include <otto/math/math.h>

namespace otto {

  struct Triangle
  {
    Vector2_F32 v1;
    Vector2_F32 v2;
    Vector2_F32 v3;
  };

  struct Triangle3
  {
    Vector3_F32 v1;
    Vector3_F32 v2;
    Vector3_F32 v3;
  };

}  // otto
