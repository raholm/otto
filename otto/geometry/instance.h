#pragma once

#include "model.h"
#include "transform.h"

namespace otto {

  struct Instance
  {
    const Model* model { nullptr };
    Transform transform;
  };

}  // otto
