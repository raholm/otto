#include <cmath>

#include "model.h"

namespace otto {

  Model GetCubeModel()
  {
    Model result;
    result.vertices = {
      { 0.5f, 0.5f, 0.5f },
      { -0.5f, 0.5f, 0.5f },
      { -0.5f, -0.5f, 0.5f },
      { 0.5f, -0.5f, 0.5f },
      { 0.5f, 0.5f, -0.5f },
      { -0.5f, 0.5f, -0.5f },
      { -0.5f, -0.5f, -0.5f },
      { 0.5f, -0.5f, -0.5f },
    };

    result.triangles = {
      { 0, 1, 2 },
      { 0, 2, 3 },
      { 4, 0, 3 },
      { 4, 3, 7 },
      { 5, 4, 7 },
      { 5, 7, 6 },
      { 1, 5, 6 },
      { 1, 6, 2 },
      { 4, 5, 1 },
      { 4, 1, 0 },
      { 2, 6, 7 },
      { 2, 7, 3 },
    };

    return result;
  }

  Model GetSphereModel()
  {
    // @note: We can improve quality be increasing these values
    const unsigned int latitude_line_count = 16;
    const unsigned int longitude_line_count = 16;

    const float radius = 0.5f;
    const unsigned int vertex_count = latitude_line_count * (longitude_line_count + 1) + 2;

    Model result;
    result.vertices.resize(vertex_count);

    result.vertices[0] = { 0.0f, radius, 0.0f };
    result.vertices[vertex_count - 1] = { 0, -radius, 0 };

    const float latitude_spacing = 1.0f / (latitude_line_count + 1.0f);
    const float longitude_spacing = 1.0f / (longitude_line_count);

    unsigned int vertex_index = 1;

    for (unsigned int latitude = 0; latitude < latitude_line_count; ++latitude)
    {
      for (unsigned int longitude = 0; longitude <= longitude_line_count; ++longitude)
      {
        const float theta = (longitude * longitude_spacing) * 2.0f * M_PI;
        const float phi = ((1.0f - (latitude + 1) * latitude_spacing) - 0.5f) * M_PI;

        const float cos_phi = std::cos(phi);

        result.vertices[vertex_index] = radius * Vector3_F32 {
          cos_phi * std::cos(theta),
          std::sin(phi),
          cos_phi * std::sin(theta)
        };

        vertex_index++;
      }
    }

    const unsigned int triangle_count = latitude_line_count * longitude_line_count * 2;

    result.triangles.resize(triangle_count);

    unsigned int triangle_index = 0;

    for (unsigned longitude = 0; longitude < longitude_line_count; ++longitude)
    {
      result.triangles[triangle_index] = { 0, longitude + 2, longitude + 1 };
      triangle_index++;
    }

    int row_length = longitude_line_count +1;

    for (unsigned int latitude = 0; latitude < latitude_line_count - 1; ++latitude)
    {
      unsigned int row_start = latitude * row_length + 1;

      for (unsigned int longitude = 0; longitude < longitude_line_count; ++longitude)
      {
        const unsigned int first_corner = row_start + longitude;

        result.triangles[triangle_index] = { first_corner, first_corner + row_length + 1, first_corner + row_length };
        triangle_index++;

        result.triangles[triangle_index] = { first_corner, first_corner + 1, first_corner + row_length + 1 };
        triangle_index++;
      }
    }

    const unsigned int pole = result.vertices.size() - 1;
    const unsigned int bottom_row = (latitude_line_count - 1) * row_length + 1;

    for (unsigned int longitude = 0; longitude < longitude_line_count; ++longitude)
    {
      result.triangles[triangle_index] = { pole, bottom_row + longitude, bottom_row + longitude + 1 };
      triangle_index++;
    }

    return result;
  }

}  // otto
