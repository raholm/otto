#include <cmath>

#include "transform.h"

namespace otto {

  float Transform::Rotation() const
  {
    return M_PI * rotation / 180.0f;
  }

}  // otto
