#include <otto/debug/debug.h>
#include <otto/rasterizer/rasterizer.h>

void DrawCube(otto::Rasterizer& rasterizer)
{
  const otto::Vector3_F32 front1 =  { -0.5f, 0.5f, 2.0f };
  const otto::Vector3_F32 front2 = { 0.5f, 0.5f, 2.0f };
  const otto::Vector3_F32 front3 = { 0.5f, -0.5f, 2.0f };
  const otto::Vector3_F32 front4 = { -0.5f, -0.5f, 2.0f };

  const otto::Vector3_F32 back1 = { -0.5f, 0.5f, 4.0f };
  const otto::Vector3_F32 back2 = { 0.5f, 0.5f, 4.0f };
  const otto::Vector3_F32 back3 = { 0.5f, -0.5f, 4.0f };
  const otto::Vector3_F32 back4 = { -0.5f, -0.5f, 4.0f };

  const otto::Color red = { 255, 0, 0 };
  const otto::Color green = { 0, 255, 0 };
  const otto::Color blue = { 0, 0, 255 };

  // Front
  {
    const otto::Line line = {
      ConvertWorldToCanvas(front1, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(front2, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, blue);
  }

  {
    const otto::Line line = {
      ConvertWorldToCanvas(front2, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(front3, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, blue);
  }

  {
    const otto::Line line = {
      ConvertWorldToCanvas(front3, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(front4, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, blue);
  }

  {
    const otto::Line line = {
      ConvertWorldToCanvas(front4, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(front1, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, blue);
  }

  // Back
  {
    const otto::Line line = {
      ConvertWorldToCanvas(back1, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(back2, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, red);
  }

  {
    const otto::Line line = {
      ConvertWorldToCanvas(back2, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(back3, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, red);
  }

  {
    const otto::Line line = {
      ConvertWorldToCanvas(back3, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(back4, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, red);
  }

  {
    const otto::Line line = {
      ConvertWorldToCanvas(back4, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(back1, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, red);
  }

  // Side
  {
    const otto::Line line = {
      ConvertWorldToCanvas(front1, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(back1, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, green);
  }

  {
    const otto::Line line = {
      ConvertWorldToCanvas(front2, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(back2, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, green);
  }

  {
    const otto::Line line = {
      ConvertWorldToCanvas(front3, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(back3, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, green);
  }

  {
    const otto::Line line = {
      ConvertWorldToCanvas(front4, rasterizer.canvas, rasterizer.viewport),
      ConvertWorldToCanvas(back4, rasterizer.canvas, rasterizer.viewport),
    };

    rasterizer.Draw(line, green);
  }
}

void BasicShapes(otto::Rasterizer& rasterizer)
{
  const otto::Line line1 = {
    { 100.0f, 240.0f },
    { 540.0f, 360.0f }
  };

  const otto::Line line2 = {
    { 310.0f, 100.0f },
    { 330.0f, 500.0f }
  };

  const otto::Triangle triangle1 = {
    { 200.0f, 300.0f },
    { 250.0f, 400.0f },
    { 300.0f, 350.0f },
  };

  const otto::Triangle triangle2 = {
    { 400.0f, 150.0f },
    { 620.0f, 400.0f },
    { 600.0f, 150.0f },
  };

  const otto::Triangle triangle3 = {
    { 20.0f, 20.0f },
    { 125.0f, 125.0f },
    { 300.0f, 75.0f },
  };

  rasterizer.Draw(line1, { 255, 0, 0 });
  rasterizer.Draw(line2, { 255, 0, 255 });
  rasterizer.DrawWireframe(triangle1, { 255, 255, 0 });
  rasterizer.DrawFilled(triangle2, { 0, 255, 0 });
  rasterizer.DrawWireframe(triangle2, { 255, 255, 255 });
  rasterizer.DrawShaded(triangle3, { 1.0f, 0.0f, 0.5f }, { 0, 0, 255 });

  DrawCube(rasterizer);

  const otto::Model cube_model = otto::GetCubeModel();

  const otto::Instance cube_instance1 = {
    &cube_model,
    {
      { 3.0f, 4.0f, 10.0f },
      1.0f,
      0.0,
    }
  };

  const otto::Instance cube_instance2 = {
    &cube_model,
    {
      { -3.0f, 3.0f, 10.0f },
      1.0f,
      0.0f,
    }
  };

  const otto::Instance cube_outside_behind = {
    &cube_model,
    {
      { 0.0f, 0.0f, -10.0f },
      1.0f,
      45.0f,
    }
  };

  rasterizer.DrawWireframe(cube_instance1, { 255, 255, 0 });
  rasterizer.DrawWireframe(cube_instance2, { 255, 0, 255 });
  rasterizer.DrawWireframe(cube_outside_behind, { 255, 255, 0 });
}

void RayTracingExample(otto::Rasterizer& rasterizer)
{
  const otto::Model sphere_model = otto::GetSphereModel();

  const otto::Instance sphere_instance1 = {
    &sphere_model,
    {
      { 0.0f, -1.0f, 3.0f },
      1.0f,
      0.0f,
    }
  };

  const otto::Material sphere_material1 = {
    { 255, 0, 0 },
    500.0f,
  };

  const otto::Instance sphere_instance2 = {
    &sphere_model,
    {
      { 2.0f, 0.0f, 4.0f },
      1.0f,
      0.0f,
    }
  };

  const otto::Material sphere_material2 = {
    { 0, 0, 255 },
    500.0f,
  };

  const otto::Instance sphere_instance3 = {
    &sphere_model,
    {
      { -2.0f, 0.0f, 4.0f },
      1.0f,
      0.0f,
    }
  };

  const otto::Material sphere_material3 = {
    { 0, 255, 0 },
    10.0f,
  };

  // @todo: For some reason back face culling is fucked up for this when z = 0.0f so we have to move it forward
  const otto::Instance sphere_instance4 = {
    &sphere_model,
    {
      { 0.0f, -5001.0f, 5001.0f },
      5000.0f,
      0.0f,
    }
  };

  const otto::Material sphere_material4 = {
    { 255, 255, 0 },
    1000.0f,
  };

  const otto::SceneLight lights = {
    {
      0.2f,
    },
    {
      { 2.0f, 1.0f, 0.0f },
      0.6f,
    },
    {
      { 1.0f, 4.0f, 4.0f },
      0.2f,
    },
  };

  // @note: Wireframes
  // rasterizer.DrawWireframe(sphere_instance1, { 255, 255, 255 });
  // rasterizer.DrawWireframe(sphere_instance2, { 255, 255, 255 });
  // rasterizer.DrawWireframe(sphere_instance3, { 255, 255, 255 });
  // rasterizer.DrawWireframe(sphere_instance4, { 255, 255, 255 });

  rasterizer.DrawSphere(sphere_instance1, lights, sphere_material1);
  rasterizer.DrawSphere(sphere_instance2, lights, sphere_material2);
  rasterizer.DrawSphere(sphere_instance3, lights, sphere_material3);
  rasterizer.DrawSphere(sphere_instance4, lights, sphere_material4);
}

int main()
{
  OTTO_LOG_INFO("Welcome to otto!");

  const unsigned int width = 640;
  const unsigned int height = 640;

  otto::Rasterizer rasterizer(width, height);

  // @note: render random shapes
  // BasicShapes(rasterizer);

  RayTracingExample(rasterizer);

  rasterizer.Display();

  OTTO_LOG_FLUSH();
  return 0;
}
