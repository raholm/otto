#pragma once

#include <iostream>

namespace otto {

  struct Vector3_F32
  {
  public:
    Vector3_F32 operator-(const Vector3_F32 rhs) const;
    Vector3_F32& operator-=(const Vector3_F32 rhs);

    Vector3_F32 operator+(const Vector3_F32 rhs) const;
    Vector3_F32& operator+=(const Vector3_F32 rhs);

    Vector3_F32 operator*(const float rhs) const;
    Vector3_F32& operator*=(const float rhs);

    Vector3_F32 operator/(const float rhs) const;
    Vector3_F32& operator/=(const float rhs);

    Vector3_F32 operator-() const;

    float LengthSquared() const;
    float Length() const;

    Vector3_F32 Normalized() const;
    Vector3_F32 ReflectAround(const Vector3_F32 axis) const;
    Vector3_F32 Cross(const Vector3_F32 rhs) const;

    float Dot(const Vector3_F32 rhs) const;

  public:
    float x { 0.0f };
    float y { 0.0f };
    float z { 0.0f };
  };

  Vector3_F32 operator*(const float lhs, const Vector3_F32 rhs);

  inline std::ostream& operator<<(std::ostream& out, const Vector3_F32 rhs)
  {
    out << rhs.x << ", " << rhs.y << ", " << rhs.z;
    return out;
  }

}  // otto
