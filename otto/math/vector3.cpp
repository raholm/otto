#include <cmath>

#include "vector3.h"

namespace otto {

  Vector3_F32 Vector3_F32::operator-(const Vector3_F32 rhs) const
  {
    return { x - rhs.x, y - rhs.y, z - rhs.z };
  }

  Vector3_F32& Vector3_F32::operator-=(const Vector3_F32 rhs)
  {
    x -= rhs.x;
    y -= rhs.y;
    z -= rhs.z;
    return *this;
  }

  Vector3_F32 Vector3_F32::operator+(const Vector3_F32 rhs) const
  {
    return { x + rhs.x, y + rhs.y, z + rhs.z };
  }

  Vector3_F32& Vector3_F32::operator+=(const Vector3_F32 rhs)
  {
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
    return *this;
  }

  Vector3_F32 Vector3_F32::operator*(const float rhs) const
  {
    return { x * rhs, y * rhs, z * rhs };
  }

  Vector3_F32& Vector3_F32::operator*=(const float rhs)
  {
    x *= rhs;
    y *= rhs;
    z *= rhs;
    return *this;
  }

  Vector3_F32 Vector3_F32::operator/(const float rhs) const
  {
    return { x / rhs, y / rhs, z / rhs };
  }

  Vector3_F32& Vector3_F32::operator/=(const float rhs)
  {
    x /= rhs;
    y /= rhs;
    z /= rhs;
    return *this;
  }

  Vector3_F32 Vector3_F32::operator-() const
  {
    return { -x, -y, -z };
  }

  float Vector3_F32::LengthSquared() const
  {
    return Dot(*this);
  }

  float Vector3_F32::Length() const
  {
    return std::sqrt(LengthSquared());
  }

  Vector3_F32 Vector3_F32::Normalized() const
  {
    const float length = Length();
    return { x / length, y / length, z / length };
  }

  float Vector3_F32::Dot(const Vector3_F32 rhs) const
  {
    return x * rhs.x + y * rhs.y + z * rhs.z;
  }

  Vector3_F32 Vector3_F32::ReflectAround(const Vector3_F32 axis) const
  {
    return 2.0f * axis * Dot(axis) - *this;
  }

  Vector3_F32 Vector3_F32::Cross(const Vector3_F32 rhs) const
  {
    return {
      y * rhs.z - z * rhs.y,
      z * rhs.x - x * rhs.z,
      x * rhs.y - y * rhs.x,
    };
  }

  Vector3_F32 operator*(const float lhs, const Vector3_F32 rhs)
  {
    return rhs * lhs;
  }

}  // otto
