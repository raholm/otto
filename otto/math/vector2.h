#pragma once

#include <iostream>

namespace otto {

  struct Vector2_F32
  {
    float x { 0.0f };
    float y { 0.0f };
  };

  inline std::ostream& operator<<(std::ostream& out, const Vector2_F32 rhs)
  {
    out << rhs.x << ", " << rhs.y;
    return out;
  }

}  // otto
