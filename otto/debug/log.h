#pragma once

#include <iostream>

#define OTTO_LOG_INFO(message) std::cout << "INFO: " << message << std::endl
#define OTTO_LOG_ERROR(message) std::cout << "ERROR: " << message << std::endl
#define OTTO_LOG_FLUSH() std::cout << std::endl;
