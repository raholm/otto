#pragma once

#include <cassert>

#define OTTO_ASSERT(expression) assert(expression)
