#pragma once

namespace otto {

  struct Color
  {
    Color operator+(const Color rhs) const;

    unsigned int red : 8;
    unsigned int green : 8;
    unsigned int blue : 8;
  };

  Color operator*(const float lhs, const Color rhs);

}  // otto
