#pragma once

#include <otto/math/math.h>
#include <otto/geometry/geometry.h>

#include "canvas.h"
#include "viewport.h"

namespace otto {

  Vector2_F32 ConvertViewportToCanvas(const Vector2_F32 point,
                                      const Canvas& canvas,
                                      const Viewport& viewport);

  Vector2_F32 ConvertWorldToCanvas(const Vector3_F32 point,
                                   const Canvas& canvas,
                                   const Viewport& viewport);

  std::vector<Vector3_F32> ConvertModelToWorld(const std::vector<Vector3_F32>& point,
                                               const Transform transform);

}  // otto
