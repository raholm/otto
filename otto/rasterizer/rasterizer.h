#pragma once

#include <array>

#include "canvas.h"
#include "color.h"
#include "viewer.h"
#include "window.h"
#include "viewport.h"
#include "conversion.h"
#include "depth_buffer.h"
#include "clipping.h"
#include "back_face_culling.h"
#include "camera.h"
#include "light.h"
#include "material.h"

namespace otto {

  struct Rasterizer
  {
  public:
    Rasterizer(const unsigned int width,
               const unsigned int height);

    void Draw(const Line line, const Color color);
    void Draw(const Line3 line, const Color color);

    void DrawWireframe(const Triangle triangle, const Color color);
    void DrawWireframe(const Triangle3 triangle, const Color color);
    void DrawWireframe(const Instance& instance, const Color color);

    void DrawSphere(const Instance& instance,
                    const SceneLight& lights,
                    const Material material);
    void DrawSphereTriangle(const Triangle3 triangle,
                            const Triangle3 normal,
                            const Triangle3 world,
                            const SceneLight& lights,
                            const Material material);

    void DrawFilled(const Triangle triangle, const Color color);

    void DrawShaded(const Triangle triangle,
                    const std::array<float, 3> weights,
                    const Color color);

    void Display() const;

  public:
    Canvas canvas;
    Camera camera;
    Viewport viewport;
    DepthBuffer depth_buffer;
    ClippingPlanes clipping_planes;

  };

}  // otto
