#include <cmath>

#include <otto/debug/debug.h>

#include "conversion.h"

namespace otto {

  Vector2_F32 ConvertViewportToCanvas(const Vector2_F32 point,
                                      const Canvas& canvas,
                                      const Viewport& viewport)
  {
    return {
      (point.x + viewport.width / 2.0f) *
      static_cast<float>(canvas.width) /
      viewport.width,
      (point.y + viewport.height / 2.0f) *
      static_cast<float>(canvas.height) /
      viewport.height,
    };
  }

  Vector2_F32 ConvertWorldToCanvas(const Vector3_F32 point,
                                   const Canvas& canvas,
                                   const Viewport& viewport)
  {
    return ConvertViewportToCanvas({
        point.x * viewport.depth / point.z,
        point.y * viewport.depth / point.z,
      },
      canvas,
      viewport);
  }

  std::vector<Vector3_F32> ConvertModelToWorld(const std::vector<Vector3_F32>& points,
                                               const Transform transform)
  {
    // @todo: We scale by two since we fucked up somewhere
    const float scale = 2.0f * transform.scale;
    const float cos_angle = std::cos(transform.Rotation());
    const float sin_angle = std::sin(transform.Rotation());
    const Vector3_F32 translation = transform.position;

    std::vector<Vector3_F32> result;
    result.resize(points.size());

    for (unsigned int index = 0; index < points.size(); ++index)
    {
      const Vector3_F32 point = points[index];
      const Vector3_F32 scaled_point = scale * point;
      const Vector3_F32 rotated_point = {
        cos_angle * scaled_point.x - sin_angle * scaled_point.y,
        sin_angle * scaled_point.x + cos_angle * scaled_point.y,
        scaled_point.z,
      };

      const Vector3_F32 translated_point = rotated_point + translation;

      result[index] = translated_point;
    }

    return result;
  }

}  // otto
