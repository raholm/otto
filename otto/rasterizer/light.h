#pragma once

#include <otto/math/math.h>

namespace otto {

  struct PointLight
  {
    Vector3_F32 position;
    float intensity;
  };

  struct DirectionalLight
  {
    Vector3_F32 direction;
    float intensity;
  };

  struct AmbientLight
  {
    float intensity;
  };

  struct SceneLight
  {
    AmbientLight ambient;
    PointLight point;
    DirectionalLight directional;
  };

}  // otto
