#include "back_face_culling.h"

namespace otto {

  bool IsBackFace(const Camera camera, const Triangle3 triangle)
  {
    const Vector3_F32 v1 = triangle.v2 - triangle.v1;
    const Vector3_F32 v2 = triangle.v3 - triangle.v1;
    const Vector3_F32 triangle_normal = v1.Cross(v2);
    const Vector3_F32 view_vector = camera.direction;
    return view_vector.Dot(triangle_normal) <= 0.0f;
  }

  Model BackFaceCull(const Camera camera, const Model& model)
  {
    Model result;
    result.vertices = model.vertices;
    result.triangles.reserve(model.triangles.size());

    for (const Model::Triangle triangle_indices : model.triangles)
    {
      // @note: Sphere model seems to have the opposite order from cube
      const Triangle3 triangle = {
        model.vertices[triangle_indices.v3],
        model.vertices[triangle_indices.v2],
        model.vertices[triangle_indices.v1],
      };

      if (IsBackFace(camera, triangle))
        continue;

      result.triangles.push_back(triangle_indices);
    }

    return result;
  }

}  // otto
