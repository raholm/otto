#pragma once

#include <cmath>

#include <otto/math/math.h>
#include <otto/geometry/geometry.h>

namespace otto {

  struct ClippingPlanes
  {
    struct Plane
    {
      Vector3_F32 normal;
      float constant;
    };

    enum class Status
    {
      Inside,
      Outside,
      Intersect,
    };

    static const float kInverseSqrtTwo;

    Model Clip(const Model& model) const;
    Model Clip(const Plane plane, const Model& model, const Sphere bounding_sphere) const;
    Model Clip(const Plane plane, const Model& model) const;

    Sphere ComputeBoundingSphere(const std::vector<Vector3_F32>& vertices) const;
    float ComputeSignedDistance(const Plane plane, const Vector3_F32 point) const;

    Vector3_F32 ComputeIntersection(const Plane plane, const Line3 line) const;

    Plane left { { kInverseSqrtTwo, 0.0f, kInverseSqrtTwo }, 0.0f };
    Plane right { { -kInverseSqrtTwo, 0.0f, kInverseSqrtTwo }, 0.0f };
    Plane top { { 0.0f, -kInverseSqrtTwo, kInverseSqrtTwo }, 0.0f };
    Plane bottom { { 0.0f, kInverseSqrtTwo, kInverseSqrtTwo }, 0.0f };
    Plane near { { 0.0f, 0.0f, 1.0f }, 0.0f };
  };

}  // otto
