#include <cmath>

#include "camera.h"

namespace otto {

  float Camera::Rotation() const
  {
    return M_PI * rotation / 180.0f;
  }

}  // otto"
