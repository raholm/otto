#pragma once

#include <otto/geometry/geometry.h>

#include "camera.h"

namespace otto {

  bool IsBackFace(const Camera camera, const Triangle3 triangle);

  Model BackFaceCull(const Camera camera, const Model& model);

}  // otto
