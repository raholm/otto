#pragma once

#include <vector>

namespace otto {

  struct DepthBuffer
  {
  public:
    DepthBuffer(const unsigned int width,
                const unsigned int height);

    bool IsInside(const unsigned int x,
                  const unsigned int y,
                  const float z) const;

    void Set(const unsigned int x,
             const unsigned int y,
             const float z);

    float Get(const unsigned int x,
              const unsigned int y) const;

  private:
    unsigned int width;
    unsigned int height;
    std::vector<float> depths;

  };

}  // otto
