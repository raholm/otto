#include <otto/debug/debug.h>

#include "canvas.h"

namespace otto {

  Canvas::Canvas(const unsigned int width,
                 const unsigned int height)
    : width(width),
      height(height)
  {
    pixels.resize(width * height);

    for (Color& pixel : pixels)
    {
      pixel.red = 0;
      pixel.green = 0;
      pixel.blue = 0;
    }
  }

  void Canvas::Set(const unsigned int x,
                   const unsigned int y,
                   const Color color)
  {
    // @todo: Clipping is fucked in the ass
    if (x >= width)
      return;

    if (y >= height)
      return;

    OTTO_ASSERT(x < width);
    OTTO_ASSERT(y < height);

    pixels[x + y * width] = color;
  }

  Color Canvas::Get(const unsigned int x,
                    const unsigned int y) const
  {
    // @todo: Clipping is fucked in the ass
    if (x >= width)
      return {};

    if (y >= height)
      return {};

    OTTO_ASSERT(x < width);
    OTTO_ASSERT(y < height);

    return pixels[x + y * width];
  }

}  // otto
