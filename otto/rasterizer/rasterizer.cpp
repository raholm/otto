#include <cstdlib>

#include <otto/debug/debug.h>

#include "rasterizer.h"

namespace otto {

  namespace details {

    float ComputeLightIntensity(const SceneLight& lights,
                                const Vector3_F32 point,
                                const Vector3_F32 normal,
                                const Vector3_F32 view_direction,
                                const float specularity)
    {
      const auto ComputeDiffuseIntensityFactor =
        [](const Vector3_F32 light_vector, const Vector3_F32 normal)
          {
            const float angle = normal.Dot(light_vector);

            if (angle <= 0.0f)
              return 0.0f;

            return angle / light_vector.Length();
          };

      const auto ComputeSpecularIntensityFactor =
        [](const Vector3_F32 light_vector,
           const Vector3_F32 normal,
           const Vector3_F32 view_direction,
           const float specularity)
          {
            if (specularity <= 0.0f)
              return 0.0f;

            const Vector3_F32 reflected_direction = light_vector.ReflectAround(normal).Normalized();
            const float cos_angle = reflected_direction.Dot(view_direction);

            if (cos_angle <= 0.0f)
              return 0.0f;

            return std::pow(cos_angle, specularity);
          };

      float result = lights.ambient.intensity;

      {
        const PointLight& light = lights.point;
        const Vector3_F32 light_vector = light.position - point;

        result += light.intensity * (ComputeDiffuseIntensityFactor(light_vector, normal) +
                                     ComputeSpecularIntensityFactor(light_vector, normal, view_direction, specularity));
      }

      {
        const DirectionalLight& light = lights.directional;
        const Vector3_F32 light_vector = light.direction;

        result += light.intensity * (ComputeDiffuseIntensityFactor(light_vector, normal) +
                                     ComputeSpecularIntensityFactor(light_vector, normal, view_direction, specularity));
      }

      return result;
    }

    std::vector<float> Combine(const std::vector<float>& lhs, const std::vector<float>& rhs)
    {
      std::vector<float> result;
      result.reserve(lhs.size() + rhs.size() - 1);

      for (unsigned int index = 0; index < lhs.size() - 1; ++index)
        result.push_back(lhs[index]);

      for (unsigned int index = 0; index < rhs.size(); ++index)
        result.push_back(rhs[index]);

      return result;
    }

    std::vector<float> LinearInterpolate(const Vector2_F32 start, const Vector2_F32 end)
    {
      OTTO_ASSERT(start.x <= end.x);

      if (start.x == end.x)
        return { start.y };

      std::vector<float> result;
      result.reserve(end.x - start.x);

      const float slope = (end.y - start.y) / (end.x - start.x);

      float y = start.y;

      for (float x = std::floor(start.x); x <= std::floor(end.x); x += 1.0f)
      {
        result.push_back(y);
        y += slope;
      }

      return result;
    }

  }  // details

  Rasterizer::Rasterizer(const unsigned int width,
                         const unsigned int height)
    : canvas(width, height),
      camera({ { 0.0f, 0.0f, 0.0f}, { 0.0f, 0.0f, 1.0f }, 0.0f }),
      viewport({ 1.0f, 1.0f, 1.0f }),
      depth_buffer(width, height)
  {
    clipping_planes.near.constant = -viewport.depth;
  }

  void Rasterizer::Draw(const Line line, const Color color)
  {
    Vector2_F32 start = line.start;
    Vector2_F32 end = line.end;

    if (std::abs(end.x - start.x) > std::abs(end.y - start.y))
    {
      if (start.x > end.x)
        std::swap(start, end);

      const std::vector<float> y_values =
        details::LinearInterpolate({ start.x, start.y },
                                   { end.x, end.y });

      unsigned int index = 0;

      for (float x = start.x; x <= end.x; x += 1.0f)
      {
        canvas.Set(x, y_values[index], color);
        index++;
      }
    }
    else
    {
      if (start.y > end.y)
        std::swap(start, end);

      const std::vector<float> x_values =
        details::LinearInterpolate({ start.y, start.x },
                                   { end.y, end.x });

      unsigned int index = 0;

      for (float y = start.y; y <= end.y; y += 1.0f)
      {
        canvas.Set(x_values[index], y, color);
        index++;
      }
    }
  }

  void Rasterizer::Draw(const Line3 line, const Color color)
  {
    Vector3_F32 start = line.start;
    Vector3_F32 end = line.end;

    if (std::abs(end.x - start.x) > std::abs(end.y - start.y))
    {
      if (start.x > end.x)
        std::swap(start, end);

      const std::vector<float> y_values =
        details::LinearInterpolate({ start.x, start.y },
                                   { end.x, end.y });
      const std::vector<float> z_values =
        details::LinearInterpolate({ start.x, start.z },
                                   { end.x, end.z });

      unsigned int index = 0;

      for (float x = start.x; x <= end.x; x += 1.0f)
      {
        const float y = y_values[index];
        const float z = z_values[index];

        if (depth_buffer.IsInside(x, y, z))
        {
          canvas.Set(x, y_values[index], color);
          depth_buffer.Set(x, y, z);
        }

        index++;
      }
    }
    else
    {
      if (start.y > end.y)
        std::swap(start, end);

      const std::vector<float> x_values =
        details::LinearInterpolate({ start.y, start.x },
                                   { end.y, end.x });
      const std::vector<float> z_values =
        details::LinearInterpolate({ start.y, start.z },
                                   { end.y, end.z });

      unsigned int index = 0;

      for (float y = start.y; y <= end.y; y += 1.0f)
      {
        const float x = x_values[index];
        const float z = z_values[index];

        if (depth_buffer.IsInside(x, y, z))
        {
          canvas.Set(x, y, color);
          depth_buffer.Set(x, y, z);
        }

        index++;
      }
    }
  }

  void Rasterizer::DrawWireframe(const Triangle triangle, const Color color)
  {
    Draw(Line { triangle.v1, triangle.v2 }, color);
    Draw(Line { triangle.v1, triangle.v3 }, color);
    Draw(Line { triangle.v2, triangle.v3 }, color);
  }

  void Rasterizer::DrawWireframe(const Triangle3 triangle, const Color color)
  {
    Draw(Line3 { triangle.v1, triangle.v2 }, color);
    Draw(Line3 { triangle.v1, triangle.v3 }, color);
    Draw(Line3 { triangle.v2, triangle.v3 }, color);
  }

  void Rasterizer::DrawWireframe(const Instance& instance,
                                 const Color color)
  {
    OTTO_ASSERT(instance.model);

    Transform transform = instance.transform;
    transform.rotation -= camera.rotation;
    transform.position -= camera.position;

    const std::vector<Vector3_F32> world_vertices = ConvertModelToWorld(instance.model->vertices, transform);

    const Model clipped_model = clipping_planes.Clip(Model { world_vertices, instance.model->triangles });

    if (clipped_model.triangles.empty())
      return;

    std::vector<Vector2_F32> projected_vertices;
    projected_vertices.reserve(clipped_model.vertices.size());

    for (const Vector3_F32 vertex : clipped_model.vertices)
    {
      projected_vertices.emplace_back(ConvertWorldToCanvas(vertex,
                                                           canvas,
                                                           viewport));
    }

    for (const Model::Triangle triangle_indices : clipped_model.triangles)
    {
      const Triangle3 triangle = {
        { projected_vertices[triangle_indices.v1].x, projected_vertices[triangle_indices.v1].y, clipped_model.vertices[triangle_indices.v1].z },
        { projected_vertices[triangle_indices.v2].x, projected_vertices[triangle_indices.v2].y, clipped_model.vertices[triangle_indices.v2].z },
        { projected_vertices[triangle_indices.v3].x, projected_vertices[triangle_indices.v3].y, clipped_model.vertices[triangle_indices.v3].z },
      };

      DrawWireframe(triangle, color);
    }
  }

  void Rasterizer::DrawSphere(const Instance& instance,
                              const SceneLight& lights,
                              const Material material)
  {
    OTTO_ASSERT(instance.model);

    Transform transform = instance.transform;
    transform.rotation -= camera.rotation;
    transform.position -= camera.position;

    const std::vector<Vector3_F32> world_vertices = ConvertModelToWorld(instance.model->vertices, transform);

    const Model clipped_model = clipping_planes.Clip(Model { world_vertices, instance.model->triangles });

    if (clipped_model.triangles.empty())
    {
      OTTO_LOG_INFO("Clipped whole model!");
      return;
    }

    const Model back_face_culled_model = BackFaceCull(camera, clipped_model);

    if (back_face_culled_model.triangles.empty())
    {
      OTTO_LOG_INFO("Back face culled whole model!");
      return;
    }

    std::vector<Vector3_F32> normals;
    normals.reserve(back_face_culled_model.vertices.size());

    for (const Vector3_F32 vertex : back_face_culled_model.vertices)
      normals.emplace_back(vertex - transform.position);

    std::vector<Vector2_F32> projected_vertices;
    projected_vertices.reserve(back_face_culled_model.vertices.size());

    for (const Vector3_F32 vertex : back_face_culled_model.vertices)
    {
      projected_vertices.emplace_back(ConvertWorldToCanvas(vertex,
                                                           canvas,
                                                           viewport));
    }

    for (const Model::Triangle triangle_indices : back_face_culled_model.triangles)
    {
      const Triangle3 triangle = {
        { projected_vertices[triangle_indices.v1].x, projected_vertices[triangle_indices.v1].y, back_face_culled_model.vertices[triangle_indices.v1].z },
        { projected_vertices[triangle_indices.v2].x, projected_vertices[triangle_indices.v2].y, back_face_culled_model.vertices[triangle_indices.v2].z },
        { projected_vertices[triangle_indices.v3].x, projected_vertices[triangle_indices.v3].y, back_face_culled_model.vertices[triangle_indices.v3].z },
      };

      const Triangle3 normal = {
        normals[triangle_indices.v1],
        normals[triangle_indices.v2],
        normals[triangle_indices.v3],
      };

      const Triangle3 world = {
        back_face_culled_model.vertices[triangle_indices.v1],
        back_face_culled_model.vertices[triangle_indices.v2],
        back_face_culled_model.vertices[triangle_indices.v3],
      };

      DrawSphereTriangle(triangle, normal, world, lights, material);
    }
  }

  void Rasterizer::Display() const
  {
    Viewer(canvas).Show();
  }

  void Rasterizer::DrawFilled(const Triangle triangle, const Color color)
  {
    Vector2_F32 v1 = triangle.v1;
    Vector2_F32 v2 = triangle.v2;
    Vector2_F32 v3 = triangle.v3;

    if (v2.y < v1.y)
      std::swap(v1, v2);

    if (v3.y < v1.y)
      std::swap(v1, v3);

    if (v3.y < v2.y)
      std::swap(v2, v3);

    const std::vector<float> x_values12 = details::LinearInterpolate({ v1.y, v1.x }, { v2.y, v2.x });
    const std::vector<float> x_values23 = details::LinearInterpolate({ v2.y, v2.x }, { v3.y, v3.x });
    const std::vector<float> x_values13 = details::LinearInterpolate({ v1.y, v1.x }, { v3.y, v3.x });

    std::vector<float> x_values123;
    x_values123.reserve(x_values12.size() + x_values23.size() - 1);

    for (unsigned int index = 0; index < x_values12.size() - 1; ++index)
      x_values123.push_back(x_values12[index]);

    for (unsigned int index = 0; index < x_values23.size(); ++index)
      x_values123.push_back(x_values23[index]);

    const unsigned int middle_index = x_values123.size() / 2;

    const std::vector<float>* x_start = nullptr;
    const std::vector<float>* x_end = nullptr;

    if (x_values13[middle_index] < x_values123[middle_index])
    {
      x_start = &x_values13;
      x_end = &x_values123;
    }
    else
    {
      x_start = &x_values123;
      x_end = &x_values13;
    }

    unsigned int x_index = 0;

    for (float y = v1.y; y <= v3.y; y += 1.0f)
    {
      for (float x = x_start->at(x_index); x <= x_end->at(x_index); x += 1.0f)
      {
        canvas.Set(x, y, color);
      }

      x_index++;
    }
  }

  void Rasterizer::DrawSphereTriangle(const Triangle3 triangle,
                                      const Triangle3 normal,
                                      const Triangle3 world,
                                      const SceneLight& lights,
                                      const Material material)
  {
    Vector3_F32 v1 = triangle.v1;
    Vector3_F32 v2 = triangle.v2;
    Vector3_F32 v3 = triangle.v3;

    Vector3_F32 n1 = normal.v1;
    Vector3_F32 n2 = normal.v2;
    Vector3_F32 n3 = normal.v3;

    Vector3_F32 w1 = world.v1;
    Vector3_F32 w2 = world.v2;
    Vector3_F32 w3 = world.v3;

    if (v2.y < v1.y)
    {
      std::swap(v1, v2);
      std::swap(n1, n2);
      std::swap(w1, w2);
    }

    if (v3.y < v1.y)
    {
      std::swap(v1, v3);
      std::swap(n1, n3);
      std::swap(w1, w3);
    }

    if (v3.y < v2.y)
    {
      std::swap(v2, v3);
      std::swap(n2, n3);
      std::swap(w2, w3);
    }

    const std::vector<float> x_values12 = details::LinearInterpolate({ v1.y, v1.x }, { v2.y, v2.x });
    const std::vector<float> x_values23 = details::LinearInterpolate({ v2.y, v2.x }, { v3.y, v3.x });
    const std::vector<float> x_values13 = details::LinearInterpolate({ v1.y, v1.x }, { v3.y, v3.x });
    const std::vector<float> x_values123 = details::Combine(x_values12, x_values23);

    const std::vector<float> z_values12 = details::LinearInterpolate({ v1.y, v1.z }, { v2.y, v2.z });
    const std::vector<float> z_values23 = details::LinearInterpolate({ v2.y, v2.z }, { v3.y, v3.z });
    const std::vector<float> z_values13 = details::LinearInterpolate({ v1.y, v1.z }, { v3.y, v3.z });
    const std::vector<float> z_values123 = details::Combine(z_values12, z_values23);

    const std::vector<float> normal_x_values12 = details::LinearInterpolate({ v1.y, n1.x }, { v2.y, n2.x });
    const std::vector<float> normal_x_values23 = details::LinearInterpolate({ v2.y, n2.x }, { v3.y, n3.x });
    const std::vector<float> normal_x_values13 = details::LinearInterpolate({ v1.y, n1.x }, { v3.y, n3.x });
    const std::vector<float> normal_x_values123 = details::Combine(normal_x_values12, normal_x_values23);

    const std::vector<float> normal_y_values12 = details::LinearInterpolate({ v1.y, n1.y }, { v2.y, n2.y });
    const std::vector<float> normal_y_values23 = details::LinearInterpolate({ v2.y, n2.y }, { v3.y, n3.y });
    const std::vector<float> normal_y_values13 = details::LinearInterpolate({ v1.y, n1.y }, { v3.y, n3.y });
    const std::vector<float> normal_y_values123 = details::Combine(normal_y_values12, normal_y_values23);

    const std::vector<float> normal_z_values12 = details::LinearInterpolate({ v1.y, n1.z }, { v2.y, n2.z });
    const std::vector<float> normal_z_values23 = details::LinearInterpolate({ v2.y, n2.z }, { v3.y, n3.z });
    const std::vector<float> normal_z_values13 = details::LinearInterpolate({ v1.y, n1.z }, { v3.y, n3.z });
    const std::vector<float> normal_z_values123 = details::Combine(normal_z_values12, normal_z_values23);

    const std::vector<float> world_x_values12 = details::LinearInterpolate({ v1.y, w1.x }, { v2.y, w2.x });
    const std::vector<float> world_x_values23 = details::LinearInterpolate({ v2.y, w2.x }, { v3.y, w3.x });
    const std::vector<float> world_x_values13 = details::LinearInterpolate({ v1.y, w1.x }, { v3.y, w3.x });
    const std::vector<float> world_x_values123 = details::Combine(world_x_values12, world_x_values23);

    const std::vector<float> world_y_values12 = details::LinearInterpolate({ v1.y, w1.y }, { v2.y, w2.y });
    const std::vector<float> world_y_values23 = details::LinearInterpolate({ v2.y, w2.y }, { v3.y, w3.y });
    const std::vector<float> world_y_values13 = details::LinearInterpolate({ v1.y, w1.y }, { v3.y, w3.y });
    const std::vector<float> world_y_values123 = details::Combine(world_y_values12, world_y_values23);

    const std::vector<float> world_z_values12 = details::LinearInterpolate({ v1.y, w1.z }, { v2.y, w2.z });
    const std::vector<float> world_z_values23 = details::LinearInterpolate({ v2.y, w2.z }, { v3.y, w3.z });
    const std::vector<float> world_z_values13 = details::LinearInterpolate({ v1.y, w1.z }, { v3.y, w3.z });
    const std::vector<float> world_z_values123 = details::Combine(world_z_values12, world_z_values23);

    const unsigned int middle_index = x_values123.size() / 2;

    const std::vector<float>* x_start = nullptr;
    const std::vector<float>* x_end = nullptr;

    const std::vector<float>* z_start = nullptr;
    const std::vector<float>* z_end = nullptr;

    const std::vector<float>* normal_x_start = nullptr;
    const std::vector<float>* normal_x_end = nullptr;

    const std::vector<float>* normal_y_start = nullptr;
    const std::vector<float>* normal_y_end = nullptr;

    const std::vector<float>* normal_z_start = nullptr;
    const std::vector<float>* normal_z_end = nullptr;

    const std::vector<float>* world_x_start = nullptr;
    const std::vector<float>* world_x_end = nullptr;

    const std::vector<float>* world_y_start = nullptr;
    const std::vector<float>* world_y_end = nullptr;

    const std::vector<float>* world_z_start = nullptr;
    const std::vector<float>* world_z_end = nullptr;

    if (x_values13[middle_index] < x_values123[middle_index])
    {
      x_start = &x_values13;
      x_end = &x_values123;

      z_start = &z_values13;
      z_end = &z_values123;

      normal_x_start = &normal_x_values13;
      normal_x_end = &normal_x_values123;

      normal_y_start = &normal_y_values13;
      normal_y_end = &normal_y_values123;

      normal_z_start = &normal_z_values13;
      normal_z_end = &normal_z_values123;

      world_x_start = &world_x_values13;
      world_x_end = &world_x_values123;

      world_y_start = &world_y_values13;
      world_y_end = &world_y_values123;

      world_z_start = &world_z_values13;
      world_z_end = &world_z_values123;
    }
    else
    {
      x_start = &x_values123;
      x_end = &x_values13;

      z_start = &z_values123;
      z_end = &z_values13;

      normal_x_start = &normal_x_values123;
      normal_x_end = &normal_x_values13;

      normal_y_start = &normal_y_values123;
      normal_y_end = &normal_y_values13;

      normal_z_start = &normal_z_values123;
      normal_z_end = &normal_z_values13;

      world_x_start = &world_x_values123;
      world_x_end = &world_x_values13;

      world_y_start = &world_y_values123;
      world_y_end = &world_y_values13;

      world_z_start = &world_z_values123;
      world_z_end = &world_z_values13;
    }

    unsigned int x_index = 0;

    for (float y = v1.y; y <= v3.y; y += 1.0f)
    {
      const float x_start_value = x_start->at(x_index);
      const float x_end_value = x_end->at(x_index);

      if (x_start_value > x_end_value)
        continue;

      const std::vector<float> z_values =
        details::LinearInterpolate({ x_start_value, z_start->at(x_index) },
                                   { x_end_value, z_end->at(x_index) });

      const std::vector<float> normal_x_values =
        details::LinearInterpolate({ x_start_value, normal_x_start->at(x_index) },
                                   { x_end_value, normal_x_end->at(x_index) });

      const std::vector<float> normal_y_values =
        details::LinearInterpolate({ x_start_value, normal_y_start->at(x_index) },
                                   { x_end_value, normal_y_end->at(x_index) });

      const std::vector<float> normal_z_values =
        details::LinearInterpolate({ x_start_value, normal_z_start->at(x_index) },
                                   { x_end_value, normal_z_end->at(x_index) });

      const std::vector<float> world_x_values =
        details::LinearInterpolate({ x_start_value, world_x_start->at(x_index) },
                                   { x_end_value, world_x_end->at(x_index) });

      const std::vector<float> world_y_values =
        details::LinearInterpolate({ x_start_value, world_y_start->at(x_index) },
                                   { x_end_value, world_y_end->at(x_index) });

      const std::vector<float> world_z_values =
        details::LinearInterpolate({ x_start_value, world_z_start->at(x_index) },
                                   { x_end_value, world_z_end->at(x_index) });

      unsigned int z_index = 0;

      for (float x = x_start_value; x <= x_end_value; x += 1.0f)
      {
        const float z = z_values[z_index];

        if (depth_buffer.IsInside(x, y, z))
        {
          const Vector3_F32 normal = Vector3_F32 {
            normal_x_values[z_index],
            normal_y_values[z_index],
            normal_z_values[z_index],
          }.Normalized();

          const Vector3_F32 point = Vector3_F32 {
            world_x_values[z_index],
            world_y_values[z_index],
            world_z_values[z_index],
          };

          const Vector3_F32 view_direction = (camera.position - point).Normalized();

          const float light_intensity = details::ComputeLightIntensity(lights,
                                                                       point,
                                                                       normal,
                                                                       view_direction,
                                                                       material.specularity);

          canvas.Set(x, y, light_intensity * material.color);
          depth_buffer.Set(x, y, z);
        }

        z_index++;
      }

      x_index++;
    }
  }

  void Rasterizer::DrawShaded(const Triangle triangle,
                              const std::array<float, 3> weights,
                              const Color color)
  {
    Vector2_F32 v1 = triangle.v1;
    Vector2_F32 v2 = triangle.v2;
    Vector2_F32 v3 = triangle.v3;

    float weight1 = weights[0];
    float weight2 = weights[1];
    float weight3 = weights[2];

    if (v2.y < v1.y)
    {
      std::swap(v1, v2);
      std::swap(weight1, weight2);
    }

    if (v3.y < v1.y)
    {
      std::swap(v1, v3);
      std::swap(weight1, weight3);
    }

    if (v3.y < v2.y)
    {
      std::swap(v2, v3);
      std::swap(weight2, weight3);
    }

    const std::vector<float> x_values12 = details::LinearInterpolate({ v1.y, v1.x }, { v2.y, v2.x });
    const std::vector<float> weight_values12 = details::LinearInterpolate({ v1.y, weight1 }, { v2.y, weight2 });

    const std::vector<float> x_values23 = details::LinearInterpolate({ v2.y, v2.x }, { v3.y, v3.x });
    const std::vector<float> weight_values23 = details::LinearInterpolate({ v2.y, weight2 }, { v3.y, weight3 });

    const std::vector<float> x_values13 = details::LinearInterpolate({ v1.y, v1.x }, { v3.y, v3.x });
    const std::vector<float> weight_values13 = details::LinearInterpolate({ v1.y, weight1 }, { v3.y, weight3 });

    std::vector<float> x_values123;
    x_values123.reserve(x_values12.size() + x_values23.size() - 1);

    for (unsigned int index = 0; index < x_values12.size() - 1; ++index)
      x_values123.push_back(x_values12[index]);

    for (unsigned int index = 0; index < x_values23.size(); ++index)
      x_values123.push_back(x_values23[index]);

    std::vector<float> weight_values123;
    weight_values123.reserve(weight_values12.size() + weight_values23.size() - 1);

    for (unsigned int index = 0; index < weight_values12.size() - 1; ++index)
      weight_values123.push_back(weight_values12[index]);

    for (unsigned int index = 0; index < weight_values23.size(); ++index)
      weight_values123.push_back(weight_values23[index]);

    const unsigned int middle_index = x_values123.size() / 2;

    const std::vector<float>* x_start = nullptr;
    const std::vector<float>* x_end = nullptr;

    const std::vector<float>* weight_start = nullptr;
    const std::vector<float>* weight_end = nullptr;

    if (x_values13[middle_index] < x_values123[middle_index])
    {
      x_start = &x_values13;
      x_end = &x_values123;

      weight_start = &weight_values13;
      weight_end = &weight_values123;
    }
    else
    {
      x_start = &x_values123;
      x_end = &x_values13;

      weight_start = &weight_values123;
      weight_end = &weight_values13;
    }

    unsigned int x_index = 0;

    for (float y = v1.y; y <= v3.y; y += 1.0f)
    {
      const float x_start_value = x_start->at(x_index);
      const float x_end_value = x_end->at(x_index);

      const std::vector<float> weight_values =
        details::LinearInterpolate({ x_start_value, weight_start->at(x_index) },
                                   { x_end_value, weight_end->at(x_index) });

      unsigned int weight_index = 0;

      for (float x = x_start_value; x <= x_end_value; x += 1.0f)
      {
        const Color shaded_color = weight_values[weight_index] * color;
        canvas.Set(x, y, shaded_color);
        weight_index++;
      }

      x_index++;
    }
  }

}  // otto
