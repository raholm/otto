#pragma once

#include <otto/math/math.h>

namespace otto {

  struct Camera
  {
    float Rotation() const;

    Vector3_F32 position;
    Vector3_F32 direction;
    float rotation;
  };

}  // otto
