#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>

namespace otto {

  struct Window
  {
  public:
    Window(const unsigned int width,
           const unsigned int height);

    ~Window();

    bool ShouldClose() const;

    void SwapBuffers() const;

    void ProcessEvents() const;

  public:
    GLFWwindow* handle;

  };

}  // otto
