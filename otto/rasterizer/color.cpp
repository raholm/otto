#include "color.h"

namespace otto {

  namespace details {

    float Clamp(const float value)
    {
      if (value < 0.0f) return 0.0f;
      if (value > 255.0f) return 255.0f;
      return value;
    }

    unsigned int Clamp_U32(const unsigned int value)
    {
      if (value > 255) return 255;
      return value;
    }

  }  // details

  Color Color::operator+(const Color rhs) const
  {
    return {
      details::Clamp_U32(red + rhs.red),
      details::Clamp_U32(green + rhs.green),
      details::Clamp_U32(blue + rhs.blue),
    };
  }

  Color operator*(const float lhs, const Color rhs)
  {
    Color result;
    result.red = static_cast<unsigned int>(details::Clamp(lhs * static_cast<float>(rhs.red)));
    result.green = static_cast<unsigned int>(details::Clamp(lhs * static_cast<float>(rhs.green)));
    result.blue = static_cast<unsigned int>(details::Clamp(lhs * static_cast<float>(rhs.blue)));
    return result;
  }

}  // otto
