#include <otto/debug/debug.h>

#include "depth_buffer.h"

namespace otto {

  DepthBuffer::DepthBuffer(const unsigned int width,
                           const unsigned int height)
    : width(width),
      height(height)
  {
    depths.resize(width * height);

    for (unsigned int index = 0; index < depths.size(); ++index)
      depths[index] = 0.0f;
  }

  bool DepthBuffer::IsInside(const unsigned int x,
                             const unsigned int y,
                             const float z) const
  {
    const float value = Get(x, y);
    return 1.0f / z > value;
  }

  void DepthBuffer::Set(const unsigned int x,
                        const unsigned int y,
                        const float z)
  {
    // @todo: Clipping is fucked in the ass
    if (x >= width)
      return;

    if (y >= height)
      return;

    OTTO_ASSERT(x < width);
    OTTO_ASSERT(y < height);
    depths[x + y * width] = 1.0f / z;
  }

  float DepthBuffer::Get(const unsigned int x,
                         const unsigned int y) const
  {
    // @todo: Clipping is fucked in the ass
    if (x >= width)
      return 0.0f;

    if (y >= height)
      return 0.0f;

    OTTO_ASSERT(x < width);
    OTTO_ASSERT(y < height);
    return depths[x + y * width];
  }

}  // otto
