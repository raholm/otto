#pragma once

#include "color.h"

namespace otto {

  struct Material
  {
    Color color;
    float specularity;
  };

}  // otto
