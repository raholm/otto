#pragma once

#include "window.h"
#include "canvas.h"

namespace otto {

  struct Viewer
  {
  public:
    Viewer(const Canvas& canvas);

    void Show();

  public:
    Window window;
    const Canvas& canvas;

  };

}  // otto
