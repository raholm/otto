#include <otto/math/math.h>

#include "viewer.h"

namespace otto {

  const char* kFragmentShaderSource = R""""(
#version 460 core

    layout (location = 0) in vec4 in_color;

    layout (location = 0) out vec4 out_color;

    void main()
    {
      out_color = in_color;
    }
    )"""";

  const char* kVertexShaderSource = R""""(
#version 460 core

    layout (location = 0) in vec2 in_position;
    layout (location = 1) in vec3 in_color;

    layout (location = 0) out vec4 out_color;

    void main()
    {
      gl_Position = vec4(in_position, 0.0, 1.0);
      gl_PointSize = 1.0;
      out_color = vec4(in_color, 1.0);
    }
    )"""";

  Viewer::Viewer(const Canvas& canvas)
    : window(canvas.width, canvas.height),
      canvas(canvas)
  {

  }

  void Viewer::Show()
  {
    const unsigned int pixel_count = canvas.width * canvas.height;

    const GLuint shader_program = glCreateProgram();

    {
      const GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
      glShaderSource(vertex_shader, 1, &kVertexShaderSource, 0);

      const GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
      glShaderSource(fragment_shader, 1, &kFragmentShaderSource, 0);

      glAttachShader(shader_program, vertex_shader);
      glAttachShader(shader_program, fragment_shader);
      glLinkProgram(shader_program);
      glUseProgram(shader_program);

      glDeleteShader(vertex_shader);
      glDeleteShader(fragment_shader);
    }

    struct Vertex
    {
      Vector2_F32 position;
      Color color;
    };

    const size_t vertex_size = sizeof(Vertex);

    std::vector<Vertex> vertices;
    vertices.reserve(pixel_count);

    for (unsigned int row = 0; row < canvas.height; ++row)
    {
      for (unsigned int col = 0; col < canvas.width; ++col)
      {
        // @note convert from screen coordinates to clip coordinates
        Vector2_F32 position {
          2.0f * static_cast<float>(col) / static_cast<float>(canvas.width) - 1.0f,
          2.0f * static_cast<float>(row) / static_cast<float>(canvas.height) - 1.0f
        };

        vertices.push_back({ position, canvas.Get(col, row) });
      }
    }

    GLuint vertex_array_object;
    glGenVertexArrays(1, &vertex_array_object);

    glBindVertexArray(vertex_array_object);

    GLuint vertex_buffer_object;
    glGenBuffers(1, &vertex_buffer_object);

    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * vertex_size, vertices.data(), GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, vertex_size, (void*) 0);
    glEnableVertexAttribArray(0);

    // Color attribute
    glVertexAttribPointer(1, 3, GL_UNSIGNED_BYTE, GL_TRUE, vertex_size, (void*) sizeof(Vector2_F32));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    glClear(GL_COLOR_BUFFER_BIT);
    glDrawArrays(GL_POINTS, 0, pixel_count);

    window.SwapBuffers();

    while (!window.ShouldClose())
    {
      window.ProcessEvents();
    }

    glDeleteBuffers(1, &vertex_buffer_object);
    glDeleteVertexArrays(1, &vertex_array_object);
    glDeleteProgram(shader_program);
  }

}  // otto
