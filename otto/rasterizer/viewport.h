#pragma once

namespace otto {

  struct Viewport
  {
    float width { 0.0f };
    float height { 0.0f };
    float depth { 0.0f };
  };

}  // otto
