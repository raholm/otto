#include <otto/debug/debug.h>

#include "clipping.h"

namespace otto {

  const float ClippingPlanes::kInverseSqrtTwo = 1 / std::sqrt(2);

  Sphere ClippingPlanes::ComputeBoundingSphere(const std::vector<Vector3_F32>& vertices) const
  {
    Sphere bounding_sphere;

    for (const Vector3_F32 vertex : vertices)
      bounding_sphere.center += vertex;

    bounding_sphere.center /= vertices.size();

    for (const Vector3_F32 vertex : vertices)
      bounding_sphere.radius = std::max(bounding_sphere.radius, (bounding_sphere.center - vertex).Length());

    return bounding_sphere;
  }

  Model ClippingPlanes::Clip(const Model& model) const
  {
    OTTO_LOG_INFO("Clipping...");

    const Sphere bounding_sphere = ComputeBoundingSphere(model.vertices);

    Model result = model;

    for (const Plane plane : { left, right, top, bottom, near })
    {
      result = Clip(plane, result, bounding_sphere);

      if (result.vertices.empty())
        return {};
    }

    return result;
  }

  Model ClippingPlanes::Clip(const Plane plane, const Model& model, const Sphere bounding_sphere) const
  {
    const float signed_distance = ComputeSignedDistance(plane, bounding_sphere.center);

    if (signed_distance > bounding_sphere.radius)
      return model;

    if (signed_distance < -bounding_sphere.radius)
      return {};

    return Clip(plane, model);
  }

  Model ClippingPlanes::Clip(const Plane plane, const Model& model) const
  {
    Model result;
    result.vertices = model.vertices;

    for (const Model::Triangle triangle_indices : model.triangles)
    {
      const Triangle3 triangle = {
        model.vertices[triangle_indices.v1],
        model.vertices[triangle_indices.v2],
        model.vertices[triangle_indices.v3],
      };

      const float signed_distance1 = ComputeSignedDistance(plane, triangle.v1);
      const float signed_distance2 = ComputeSignedDistance(plane, triangle.v2);
      const float signed_distance3 = ComputeSignedDistance(plane, triangle.v3);

      if (signed_distance1 > 0.0f &&
          signed_distance2 > 0.0f &&
          signed_distance3 > 0.0f)
      {
        result.triangles.push_back(triangle_indices);
        continue;
      }

      if (signed_distance1 < 0.0f &&
          signed_distance2 < 0.0f &&
          signed_distance3 < 0.0f)
      {
        continue;
      }

      if (signed_distance1 > 0.0f &&
          signed_distance2 < 0.0f &&
          signed_distance3 < 0.0f)
      {
        const Vector3_F32 new_vertex1 = ComputeIntersection(plane, { triangle.v1, triangle.v2 });
        const Vector3_F32 new_vertex2 = ComputeIntersection(plane, { triangle.v1, triangle.v3 });
        const unsigned int vertex_count = result.vertices.size();

        result.triangles.push_back({ triangle_indices.v1, vertex_count, vertex_count + 1 });
        result.vertices.emplace_back(new_vertex1);
        result.vertices.emplace_back(new_vertex2);
        continue;
      }

      if (signed_distance2 > 0.0f &&
          signed_distance1 < 0.0f &&
          signed_distance3 < 0.0f)
      {
        const Vector3_F32 new_vertex1 = ComputeIntersection(plane, { triangle.v2, triangle.v1 });
        const Vector3_F32 new_vertex2 = ComputeIntersection(plane, { triangle.v2, triangle.v3 });
        const unsigned int vertex_count = result.vertices.size();

        result.triangles.push_back({ triangle_indices.v2, vertex_count, vertex_count + 1 });
        result.vertices.emplace_back(new_vertex1);
        result.vertices.emplace_back(new_vertex2);
        continue;
      }

      if (signed_distance3 > 0.0f &&
          signed_distance1 < 0.0f &&
          signed_distance2 < 0.0f)
      {
        const Vector3_F32 new_vertex1 = ComputeIntersection(plane, { triangle.v3, triangle.v1 });
        const Vector3_F32 new_vertex2 = ComputeIntersection(plane, { triangle.v3, triangle.v2 });
        const unsigned int vertex_count = result.vertices.size();

        result.triangles.push_back({ triangle_indices.v3, vertex_count, vertex_count + 1 });
        result.vertices.emplace_back(new_vertex1);
        result.vertices.emplace_back(new_vertex2);
        continue;
      }

      if (signed_distance1 < 0.0f &&
          signed_distance2 > 0.0f &&
          signed_distance3 > 0.0f)
      {
        const Vector3_F32 new_vertex1 = ComputeIntersection(plane, { triangle.v2, triangle.v1 });
        const Vector3_F32 new_vertex2 = ComputeIntersection(plane, { triangle.v3, triangle.v1 });
        const unsigned int vertex_count = result.vertices.size();

        result.triangles.push_back({ triangle_indices.v2, triangle_indices.v3, vertex_count });
        result.triangles.push_back({ vertex_count, triangle_indices.v3, vertex_count + 1 });
        result.vertices.emplace_back(new_vertex1);
        result.vertices.emplace_back(new_vertex2);
        continue;
      }

      if (signed_distance2 < 0.0f &&
          signed_distance1 > 0.0f &&
          signed_distance3 > 0.0f)
      {
        const Vector3_F32 new_vertex1 = ComputeIntersection(plane, { triangle.v1, triangle.v2 });
        const Vector3_F32 new_vertex2 = ComputeIntersection(plane, { triangle.v3, triangle.v2 });
        const unsigned int vertex_count = result.vertices.size();

        result.triangles.push_back({ triangle_indices.v1, triangle_indices.v3, vertex_count });
        result.triangles.push_back({ vertex_count, triangle_indices.v3, vertex_count + 1 });
        result.vertices.emplace_back(new_vertex1);
        result.vertices.emplace_back(new_vertex2);
        continue;
      }

      if (signed_distance3 < 0.0f &&
          signed_distance1 > 0.0f &&
          signed_distance2 > 0.0f)
      {
        const Vector3_F32 new_vertex1 = ComputeIntersection(plane, { triangle.v1, triangle.v3 });
        const Vector3_F32 new_vertex2 = ComputeIntersection(plane, { triangle.v2, triangle.v3 });
        const unsigned int vertex_count = result.vertices.size();

        result.triangles.push_back({ triangle_indices.v1, triangle_indices.v2, vertex_count });
        result.triangles.push_back({ vertex_count, triangle_indices.v2, vertex_count + 1 });
        result.vertices.emplace_back(new_vertex1);
        result.vertices.emplace_back(new_vertex2);
        continue;
      }
    }

    return result;
  }

  float ClippingPlanes::ComputeSignedDistance(const Plane plane, const Vector3_F32 point) const
  {
    return plane.normal.Dot(point) + plane.constant;
  }

  Vector3_F32 ClippingPlanes::ComputeIntersection(const Plane plane, const Line3 line) const
  {
    const Vector3_F32 direction = (line.end - line.start);
    const float t = (plane.constant - plane.normal.Dot(line.start)) / plane.normal.Dot(direction);
    return line.start + t * direction.Normalized();
  }

}  // otto
