#!/bin/sh

ROOT_PATH=`dirname $0`
BUILD_PATH=${ROOT_PATH}/build

mkdir -p ${BUILD_PATH}

cd ${BUILD_PATH}

cmake ..
make
